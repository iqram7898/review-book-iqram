const express = require("express");
const router = require("./src/routes/main");
const cors = require("cors");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./src/docs/swagger.json");

const app = express();
const port = 5000;

const options = {
    customCssUrl: "https://unpkg.com/swagger-ui-dist@4.5.0/swagger-ui.css",
};

app.use(express.json());
app.use(cors());
app.use(
    "/api-docs",
    swaggerUi.serve,
    swaggerUi.setup(swaggerDocument, options)
);
app.use("/", router);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
});

module.exports = app;
