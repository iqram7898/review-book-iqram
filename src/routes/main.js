const express = require("express");
const jwtAuth = require("../middlewares/auth");

const {
    getBuku,
    getBukuById,
    postBuku,
    updateBuku,
    deleteBuku,
    getReviewBukuById,
} = require("../controllers/bukuController");
const {
    getChat,
    getChatById,
    postChat,
    deleteChat,
    updateChat,
} = require("../controllers/chatController");
const {
    getKategoriById,
    postKategori,
    updateKategori,
    deleteKategori,
    getKategori,
} = require("../controllers/kategoriController");
const {
    getReview,
    getReviewById,
    postReview,
    updateReview,
    deleteReview,
} = require("../controllers/reviewController");
const {
    register,
    login,
    gantiPassword,
} = require("../controllers/authController");
const {
    getUser,
    getUserById,
    updateUser,
    deleteUser,
} = require("../controllers/userController");
const {
    getProfil,
    getProfilById,
    createProfil,
    updateProfil,
    deleteProfil,
} = require("../controllers/profilController");
const {
    getKomentar,
    getKomentarById,
    createKomentar,
    updateKomentar,
    deleteKomentar,
} = require("../controllers/komentarController");
const {
    getReplyKomentar,
    getReplyKomentarById,
    createReplyKomentar,
    updateReplyKomentar,
    deleteReplyKomentar,
} = require("../controllers/replyKomentarController");

const router = express.Router();

router.get("/", (req, res) => res.json("root"));

router.get("/user", jwtAuth, getUser);
router.get("/user/:id", jwtAuth, getUserById);
router.patch("/user/:id", jwtAuth, updateUser);
router.patch("/user/gantiPassword/:id", jwtAuth, gantiPassword);
router.delete("/user/:id", jwtAuth, deleteUser);

router.post("/register", register);
router.post("/login", login);

router.get("/profil", jwtAuth, getProfil);
router.get("/profil/:id", jwtAuth, getProfilById);
router.post("/profil", jwtAuth, createProfil);
router.patch("/profil/:id", jwtAuth, updateProfil);
router.delete("/profil/:id", jwtAuth, deleteProfil);

router.get("/komentar", jwtAuth, getKomentar);
router.get("/komentar/:id", jwtAuth, getKomentarById);
router.post("/komentar", jwtAuth, createKomentar);
router.patch("/komentar/:id", jwtAuth, updateKomentar);
router.delete("/komentar/:id", jwtAuth, deleteKomentar);

router.get("/replyKomentar", jwtAuth, getReplyKomentar);
router.get("/replyKomentar/:id", jwtAuth, getReplyKomentarById);
router.post("/replyKomentar", jwtAuth, createReplyKomentar);
router.patch("/replyKomentar/:id", jwtAuth, updateReplyKomentar);
router.delete("/replyKomentar/:id", jwtAuth, deleteReplyKomentar);

router.get("/kategori", jwtAuth, getKategori);
router.get("/kategori/:id", jwtAuth, getKategoriById);
router.post("/kategori", jwtAuth, postKategori);
router.patch("/kategori/:id", jwtAuth, updateKategori);
router.delete("/kategori/:id", jwtAuth, deleteKategori);

router.get("/buku", jwtAuth, getBuku);
router.get("/buku/:id", jwtAuth, getBukuById);
router.get("/buku/:id/review", jwtAuth, getReviewBukuById);
router.post("/buku", jwtAuth, postBuku);
router.patch("/buku/:id", jwtAuth, updateBuku);
router.delete("/buku/:id", jwtAuth, deleteBuku);

router.get("/review", jwtAuth, getReview);
router.get("/review/:id", jwtAuth, getReviewById);
router.post("/review", jwtAuth, postReview);
router.patch("/review/:id", jwtAuth, updateReview);
router.delete("/review/:id", jwtAuth, deleteReview);

router.get("/chat", jwtAuth, getChat);
router.get("/chat/:id/", jwtAuth, getChatById);
router.post("/chat", jwtAuth, postChat);
router.patch("/chat/:id", jwtAuth, updateChat);
router.delete("/chat/:id", jwtAuth, deleteChat);

module.exports = router;
