
const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()


const getKategori = async (req, res) => {
  const kategori = await prisma.kategori.findMany()
  res.json(kategori)
}
const getKategoriById = async (req, res) => {
    
  let {id} = req.params
       try {
     const kategori = await prisma.kategori.findUnique({
      where: {
        id: Number(id)
      }
    })
    res.json(kategori)
   } catch (error) {
    res.json(error.message)
   }
 
}
const postKategori = async (req, res) => {
    let {nama} = req.body;
    if(nama) {
      try {
        const kategori =  await prisma.kategori.create({
          data: {
           nama : nama,
          }
        })
        res.json({message : "Kategori Berhasil ditambahkan", data : kategori})
      } catch (error) {
        res.send(error.message)
      }
    } else {
        return res.status(400).json({message: "Nama kategori wajib diisi"})
    }
}

// update nilai mahasiswa
const updateKategori = async (req, res) => {
    let {nama}= req.body
    let {id}= req.params
    try {
        const kategori = await prisma.kategori.update({
        where: {
          id: Number(id)
        },
        data: {
          nama : nama
        }
      })
      res.send({message: 'Kategori Berhasil diupdate', kategori})
        } catch (error) {
      res.send({message: 'Kategori gagal diupdate', error})
  }
}

  // delete nilai mahasiswa
  const deleteKategori = async (req, res) => {
    let {id}= req.params

    try {
      const Kategori = await prisma.kategori.delete({
        where: {
          id: Number(id),
        },
      })
     res.json("Kategori Berhasil dihapus")
    } catch (error) {
      res.send(error)
    }
  }


module.exports = {
    getKategori,
    getKategoriById,
    postKategori,
    deleteKategori,
    updateKategori
}