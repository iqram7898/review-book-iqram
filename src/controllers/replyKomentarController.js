const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const getReplyKomentar = async (req, res) => {
    let replykomentars = await prisma.replykomentar.findMany();
    if (replykomentars.length > 0) {
        res.json(replykomentars);
    } else {
        res.json({ message: "Data Masih Kosong!" });
    }
};

const getReplyKomentarById = async (req, res) => {
    let { id } = req.params;
    let replykomentar = await prisma.replykomentar.findUnique({
        where: {
            id: isNaN(parseInt(id)) ? 0 : parseInt(id),
        },
    });
    if (replykomentar) {
        res.json(replykomentar);
    } else {
        res.status(404).json({ message: "Data Tidak Ditemukan" });
    }
};

const createReplyKomentar = async (req, res) => {
    let { isi, komentarId } = req.body;
    // try {
    const replykomentar = await prisma.replykomentar.create({
        data: {
            isi,
            komentarId,
        },
    });
    res.json({
        message: "ReplyKomentar Berhasil Ditambahkan!",
        replykomentar,
    });
    // } catch (error) {
    //     res.json({ message: "Error!", error });
    // }
};

const updateReplyKomentar = async (req, res) => {
    let { isi, komentarId } = req.body;
    let { id } = req.params;
    try {
        const replykomentar = await prisma.replykomentar.update({
            where: { id: isNaN(parseInt(id)) ? 0 : parseInt(id) },
            data: { isi, komentarId },
        });
        res.json({ message: "ReplyKomentar Berhasil Diubah!", replykomentar });
    } catch (error) {
        res.json({ message: "Error!" });
    }
};

const deleteReplyKomentar = async (req, res) => {
    let { id } = req.params;
    try {
        const replykomentar = await prisma.replykomentar.delete({
            where: { id: isNaN(parseInt(id)) ? 0 : parseInt(id) },
        });
        res.json({
            message: "Data ReplyKomentar Berhasil Dihapus!",
            replykomentar,
        });
    } catch (error) {
        res.json({ message: "Error!" });
    }
};

module.exports = {
    getReplyKomentar,
    getReplyKomentarById,
    createReplyKomentar,
    updateReplyKomentar,
    deleteReplyKomentar,
};
