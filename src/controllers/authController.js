const { PrismaClient } = require("@prisma/client");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const prisma = new PrismaClient();

const register = async (req, res) => {
    let { username, email, password } = req.body;

    // try {
    const hashedPassword = await bcrypt.hash(password, 10);
    const user = await prisma.user.create({
        data: {
            username,
            email,
            password: hashedPassword,
        },
    });
    const expiresIn = Number(process.env.EXPIRE_HOUR) * 3600;

    const token = jwt.sign({ userId: user.id }, process.env.TOKEN_SECRET, {
        expiresIn,
    });
    return res.json({ user: { id: user.id, username, email }, token });
    // } catch (error) {
    //     return res.status(400).json(error);
    // }
};

const login = async (req, res) => {
    let { username, email, password } = req.body;

    const users = await prisma.user.findMany({
        where: {
            OR: [{ username }, { email }],
        },
    });

    try {
        if (users.length > 0) {
            const user = users[0];
            const valid = await bcrypt.compare(password, user.password);
            if (valid) {
                const expiresIn = Number(process.env.EXPIRE_HOUR) * 3600;

                const token = jwt.sign(
                    { userId: user.id },
                    process.env.TOKEN_SECRET,
                    {
                        expiresIn,
                    }
                );
                return res.json({
                    user: { id: user.id, username, email },
                    token,
                });
            } else {
                return res
                    .status(400)
                    .json({ message: "Invalid Credentials!" });
            }
        } else {
            return res.status(400).json({ message: "Invalid Credentials!" });
        }
    } catch (error) {
        return res.status(400).json(error);
    }
};

const gantiPassword = async (req, res) => {
    let { username, email, oldPassword, newPassword } = req.body;
    let { id } = req.params;

    const users = await prisma.user.findMany({
        where: {
            OR: [{ username }, { email }],
        },
    });

    try {
        if (users.length > 0) {
            const user = users[0];
            const valid = await bcrypt.compare(oldPassword, user.password);
            if (valid) {
                const hashedPassword = await bcrypt.hash(newPassword, 10);
                const user = await prisma.user.update({
                    where: { id: isNaN(parseInt(id)) ? 0 : parseInt(id) },
                    data: {
                        username,
                        email,
                        password: hashedPassword,
                    },
                });

                return res.json({ message: "Password Berhasil Diubah" });
            } else {
                return res
                    .status(400)
                    .json({ message: "Invalid Credentials!" });
            }
        } else {
            return res.status(400).json({ message: "Invalid Credentials!" });
        }
    } catch (error) {
        return res.status(400).json(error);
    }
};

module.exports = {
    register,
    login,
    gantiPassword,
};
