
const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()


const getBuku = async (req, res) => {
  const buku = await prisma.buku.findMany()
  res.json(buku)
}
const getBukuById = async (req, res) => {
  let {id} = req.params
       try {
     const buku = await prisma.buku.findUnique({
      where: {
        id: Number(id)
      }
    })
    res.json(buku)
   } catch (error) {
    res.json(error.message)
   }
 
}
const getReviewBukuById = async (req, res) => {
  let {id} = req.params
       try {
     const buku = await prisma.buku.findUnique({
      where: {
        id: Number(id)
      },
      include: {
        review: true
      }
    })
    res.json(buku)
   } catch (error) {
    res.json(error.message)
   }
 
}
const postBuku = async (req, res) => {
    let {judul, sinopsis, kategori, penulis} = req.body;
    if(judul && sinopsis && kategori && penulis) {
      try {
        const buku =  await prisma.buku.create({
          data: {
           judul,
           sinopsis,
           penulis,
           kategoriId : Number(kategori)
          }
        })
        res.json({message : "buku Berhasil ditambahkan", data : buku})
      } catch (error) {
        res.send(error.message)
      }
    } else {
        return res.status(400).json({message: "Judul, sinopsis, dan kategori buku wajib diisi"})
    }
}

// update nilai mahasiswa
const updateBuku = async (req, res) => {
    let {judul, sinopsis, penulis, kategoriId}= req.body
    let {id}= req.params
    try {
        const buku = await prisma.buku.update({
        where: {
          id: Number(id)
        },
        data: {
          judul,
          sinopsis,
          penulis,
          kategoriId
        }
      })
      res.send({message: 'Buku Berhasil diupdate', buku})
        } catch (error) {
      res.send({message: 'Buku gagal diupdate', error})
  }
}

  const deleteBuku = async (req, res) => {
    let {id}= req.params

    try {
      const buku = await prisma.buku.delete({
        where: {
          id: Number(id),
        },
      })
     res.json("buku Berhasil dihapus")
    } catch (error) {
      res.send(error)
    }
  }


module.exports = {
    getBuku,
    getBukuById,
    getReviewBukuById,
    postBuku,
    deleteBuku,
    updateBuku
}